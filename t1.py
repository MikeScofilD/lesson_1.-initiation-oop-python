# Задание
# Создайте класс, описывающий автомобиль.
# Создайте класс автосалона, содержащий в себе список автомобилей,
# доступных для продажи, и функцию продажи заданного автомобиля.
# Самостоятельная деятельность учащегося

# Задание 1
# Создайте класс, описывающий книгу.
# Он должен содержать информацию об авторе,
# названии, годе издания и жанре.
# Создайте несколько разных книг.
# Определите для него операции проверки на равенство и неравенство, методы __repr__ и __str__.


class Book:
    def __init__(self, author, title, year, genre):
        self.author = author
        self.title = title
        self.year = year
        self.genre = genre

    def __eq__(self, other):
        if not isinstance(other, Book):
            return False
        else:
            return self.author == other.author and \
                   self.title == other.title and \
                   self.year == other.year and \
                   self.genre == other.genre

    def __repr__(self):
        return f"self.author = ({self.author}) \n" \
               f"self.title = ({self.title}) \n" \
               f"self.year = ({self.year}) \n" \
               f"self.genre = ({self.genre}) \n"

    def __str__(self):
        return f"self.author = ({self.author}) \n" \
               f"self.title = ({self.title}) \n" \
               f"self.year = ({self.year}) \n" \
               f"self.genre = ({self.genre}) \n"


def main():
    karenina = Book('Leo Tolstoy', 'Anna Karenina', 1873, 'Tragedy')
    # print(karenina)
    print(repr(karenina))
    hamlet = Book('William Shakespeare', 'Hamlet', 1609, 'Tragedy')
    # print(hamlet)
    print(repr(hamlet))

    print(karenina == hamlet)
    print(karenina != hamlet)
    print(karenina == karenina)
    print(karenina != karenina)
    print(hamlet == hamlet)
    print(hamlet != hamlet)


if __name__ == '__main__':
    main()

# Задание 4
# Ознакомьтесь со специальными методами в Python,
# используя ссылки в конце урока, и научитесь использовать те из них,
# назначение которых вы можете понять.
# Возвращайтесь к этой теме на протяжении всего курса и изучайте специальные методы,
# соответствующие темам каждого урока.
