class Book:
    def __init__(self, author, title, year, genre, comments=None):
        if comments is None:
            comments = []
        self.author = author
        self.title = title
        self.year = year
        self.genre = genre
        self.comments = comments

    def __eq__(self, other):
        if not isinstance(other, Book):
            return False
        else:
            return self.author == other.author and \
                   self.title == other.title and \
                   self.year == other.year and \
                   self.genre == other.genre

    def __repr__(self):
        return f"self.author = ({self.author}) \n" \
               f"self.title = ({self.title}) \n" \
               f"self.year = ({self.year}) \n" \
               f"self.genre = ({self.genre}) \n" \
               f"self.comments = ({self.comments}) \n"

    def __str__(self):
        return f"self.author = ({self.author}) \n" \
               f"self.title = ({self.title}) \n" \
               f"self.year = ({self.year}) \n" \
               f"self.genre = ({self.genre}) \n" \
               f"self.comments = ({self.comments}) \n"


# Задание 2
# Создайте класс, описывающий отзыв к книге.
# Добавьте в класс книги поле – список отзывов.
# Сделайте так, что при выводе книги на экран при помощи функции print также будут выводиться отзывы к ней.

class Comment:
    def __init__(self, mark, text):
        self.mark = mark
        self.text = text

    def __repr__(self):
        return f"\nself.mark = ({self.mark}) \n" \
               f"self.text = ({self.text}) \n"

    def __str__(self):
        return f"\nself.mark = ({self.mark}) \n" \
               f"self.text = ({self.text}) \n"


def main():
    karenina = Book('Leo Tolstoy', 'Anna Karenina', 1873, 'Tragedy')
    karenina.comments = [
        Comment(5, "best rendition I've ever seen is the 2000 series by (I think)\n"
                   "channel 4 with Helen McCrory as Anna,\n"
                   "Douglas Henshall as Levin and Mark Strong as Oblonsky.\n"
                   "Really good casting especially McCrory.\n"
                   "I first read the book in my mid\n"
                   "teens and every 3 or 4 years since and this series respects\n"
                   "all the relationships in the book.\n"
                   "Not a cheap dvd although every couple of years,\n"
                   "Yesterday channel repeats the series.\n"
                   "Hope you enjoy it\n"),
        Comment(4, "Anyone have a film rendition recommendation for Anna Karenina?\n"
                   "I was disappointed with the 1948 version starring Vivien Leigh.\n"
                   "It hardly told Levin's story at all.\n"
                   "I'm looking for something faithful to the book.\n"
                   "Is the 2012 version with Keira Knightly or the 97 version with Sophie Morceau faithful?\n"
                   "I may try the 67 Russian version (highest rating on IMDB) if I can find it with subtitles.\n"),
    ]
    # print(karenina)
    print(repr(karenina))
    hamlet = Book('William Shakespeare', 'Hamlet', 1609, 'Tragedy')
    hamlet.comments = [
        Comment(4, "Gertrude die (penalty on the relationship sinful)\n"
                   " after that I drank accidentally poisoned wine to drink basically\n"
                   " put Hamlet Hamlet arose after winning the killing of his uncle,\n"
                   " and cut off his arms and put poison into the mouth of his uncle.\n")
    ]
    # print(hamlet)
    print(repr(hamlet))

    print(karenina == hamlet)
    print(karenina != hamlet)
    print(karenina == karenina)
    print(karenina != karenina)
    print(hamlet == hamlet)
    print(hamlet != hamlet)


if __name__ == '__main__':
    main()
